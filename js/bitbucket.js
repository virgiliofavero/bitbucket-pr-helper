var pullRequest,
    openOrClosedID = 'files-summary-open-or-closed';

function getHeading(container) {
  var heading = $(container).children('.heading:eq(0)');
  return heading;
}

function getCode(container) {
  var code = $(container).children('.refract-container:eq(0)');
  return code;
}

function getFilename(container) {
  var heading = getHeading(container);
  var filename = $(heading).children('.primary').children('.filename').text()
    return filename;
}

// create element toggling function
function toggleCodeDiff(container) {
  var code = getCode(container);
  var filename = getFilename(container);
  $(code).toggle(400, function(){
    var hidden = !$(this).is(":visible");
    persistStateOfCodeDiff(filename, hidden);
  });
}

function persistStateOfCodeDiff(filename, hidden) {
  pullRequest.state[filename] = hidden;
  saveContainerForPR(pullRequest)
}

function restoreStateOfCodeDiff(container) {
  var code = getCode(container);
  var filename = getFilename(container);
  var hidden = pullRequest.state[filename]||false;
  if (hidden) {
    code.hide();
  }
}

// Files summary
function restoreStateOfFilesSummary(filesSummary) {
  var hidden = pullRequest.state[openOrClosedID]||false;
  if (hidden) {
    toggleFilesSummary(filesSummary);
  }
}

function toggleFilesSummary(filesSummary) {
  var openOrClosed = $('#' + openOrClosedID);
  $(filesSummary).toggle(400, function(){
    var hidden = !$(this).is(":visible");
    var text = ' [' + (hidden?'-':'+') + ']';
    $(openOrClosed).text(text);
    pullRequest.state[openOrClosedID] = hidden;
    saveContainerForPR(pullRequest);
  });
}

// Add toggle button on each container
function setupToggleFunctionality() {
  // Add toggle function for Files Changed
  var filesSummary = $('#commit-files-summary');
  if (typeof(filesSummary) !== 'undefined') {
    var header = filesSummary.parent('.main').children('h1');
    var openOrClosed = $('<span id="files-summary-open-or-closed"> [+]</span>');
    $(header).append(openOrClosed);
    $(header).click(function(event) {
      toggleFilesSummary(filesSummary);
    });
    restoreStateOfFilesSummary(filesSummary);
  }

  // Add toggle button to each file diff container
  $('.diff-container .heading .diff-actions').each(function() {

    // Create button
    var toggleButton = $('<button class="toggle-button aui-button aui-style">Toggle</button>');
    $(toggleButton).click(function(event) {
      var container = $(event.target).closest('.diff-container');
      toggleCodeDiff(container);
    });

    // Wrap button to conform with existing HTML structure
    var wrapper = $('<div class="aui-buttons">::after</div>')
      $(wrapper).prepend(toggleButton);
    $(this).prepend(wrapper);

    // Restore state for container
    restoreStateOfCodeDiff($(this).closest('.diff-container'))
  });
}

function getContainerId(pr) {
  return pr.team + "|" + pr.repo + "|" + pr.id;
}

function getLocalStorageContainer() {
  var container = localStorage.bitbucketHelper;
  if (typeof(container) === 'undefined') {
    var emptyObject = new Object();
    container = JSON.stringify(emptyObject);
  }
  return JSON.parse(container);
}

function getContainerForPR(pullRequest) {
  var containerId = getContainerId(pullRequest);
  var mainContainer = getLocalStorageContainer();
  if (typeof(mainContainer[containerId]) === 'undefined') {
    mainContainer[containerId] = new Object();
  }
  return mainContainer[containerId];
}

function saveContainerForPR(pullRequest) {
  var containerId = getContainerId(pullRequest);
  var mainContainer = getLocalStorageContainer();
  mainContainer[containerId] = pullRequest.state;

  localStorage.bitbucketHelper = JSON.stringify(mainContainer)
}

function injectIfPossible() {
  if ($('.diff-container .heading .diff-actions')[0] !== undefined) {
    var regex=/bitbucket\.org\/(.*)\/(.*)\/pull-requests\/([0-9]*)/;
    var meta = window.location.href.match(regex);

    pullRequest = new Object();
    pullRequest.team = meta[1];
    pullRequest.repo = meta[2];
    pullRequest.id = meta[3];

    pullRequest.state = getContainerForPR(pullRequest);

    setupToggleFunctionality();
    console.log('Initialized Bitbucket PR Helper!');
    return true;
  } else {
    setTimeout(injectIfPossible, 2000);
  }
}

injectIfPossible();
