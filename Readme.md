# Installation

The extension os not yet available as a public Chrome extension or bookmarklet. Please look at *Developer Mode Installation*

# Developer Mode Installation

To install this extension on Chrome you'll need to:

* Clone this repo or download the files
* Open Chrome
* Open a new tab and type chrome://extensions
* Make sure "Developer mode" checkbox is checked
* Click "Load unpacked extension" and select the root folder of this repo.
* Refresh any open Bitbucket PR window to load this extension

# TODO

Feel free to create issues, request features or send pull requests

* Add button to toggle all.
* Mark individual files as approved
* Clear local storage to forget about all PRs.